package de.fraunhofer.fokus.telehealth.preggyglu.model;

import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import static org.mockito.Mockito.*;

/**
 * Created by ogr on 05.09.2016.
 */
@RunWith(AndroidJUnit4.class)
public class WeightMeasurementTest extends AndroidTestCase {

    @Test
    public void testToInfluxLine() {
        // Arrange
        WeightMeasurement weightMock = mock(WeightMeasurement.class);
        when(weightMock.isFlagSet(anyInt())).thenReturn(true);
        when(weightMock.getHeightUnit()).thenReturn("m");
        when(weightMock.getUserId()).thenReturn(1);
        when(weightMock.getWeightUnit()).thenReturn("kg");
        when(weightMock.getWeight()).thenReturn(65.7);
        when(weightMock.getHeight()).thenReturn(1.78);
        when(weightMock.getBmi()).thenReturn(20.5);
        when(weightMock.getBaseTime()).thenReturn(new Date(1473061580609L));

        when(weightMock.toInfluxLine()).thenCallRealMethod();

        // Act
        String result = weightMock.toInfluxLine();

        // Assert
        String expected = "Weight,height_unit=m,user_id=1,weight_unit=kg weight=65.7,height=1.78";
        expected += ",bmi=20.5 1473061580609000000";
        assertEquals(expected, result);
    }

}
