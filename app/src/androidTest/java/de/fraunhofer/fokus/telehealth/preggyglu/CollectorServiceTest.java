package de.fraunhofer.fokus.telehealth.preggyglu;

import android.content.Intent;
import android.os.IBinder;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.AndroidTestCase;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import de.fraunhofer.fokus.telehealth.preggyglu.bluetooth.BleService;
import de.fraunhofer.fokus.telehealth.preggyglu.collection.CollectorService;
import de.fraunhofer.fokus.telehealth.preggyglu.model.GlucoseMeasurement;

import static org.mockito.Mockito.*;

/**
 * Created by ogr on 26.05.2016.
 */
@RunWith(AndroidJUnit4.class)
public class CollectorServiceTest extends AndroidTestCase {

    @ClassRule
    public static final ServiceTestRule mServiceRule = new ServiceTestRule();

    private static CollectorService collectorService;

    @BeforeClass
    public static void bindService() throws TimeoutException {
        // Create service intent
        Intent serviceIntent = new Intent(
                InstrumentationRegistry.getTargetContext(), CollectorService.class);

        // Pass data to the service start
        serviceIntent.putExtra("start", "start");

        // Bind the service and grab a reference to the binder
        IBinder binder = mServiceRule.bindService(serviceIntent);

        // Get the reference to the started service
        // CollectorServiceTest.collectorService = ((CollectorService.LocalBinder) binder).getService();
    }

    @Ignore
    @Test
    public void testDataCollectorReceiver_good() {
        // Arrange
        int counterBefore = collectorService.getDataCount();
        LocalBroadcastManager local = LocalBroadcastManager.getInstance(this.getContext());

        // Act
        // Send 3 matching broadcasts via LocalBroadcastManager
        String[] goodData = { "Glucose 100g", "Weight 89kg", "Steps 120"};
        Intent goodIntent;
        for(String field: goodData) {
            goodIntent = new Intent(BleService.ACTION_DATA_AVAILABLE);
            // goodIntent.putExtra(BleService.CHARACTERISTIC, field);
            local.sendBroadcastSync(goodIntent);
        }

        // Assert
        int expectedCounterAfter = counterBefore + goodData.length;
        int resultingCounterAfter = collectorService.getDataCount();
        assertEquals(expectedCounterAfter, resultingCounterAfter);
    }

    @Ignore
    @Test
    public void testDataCollectorReceiver_bad() {
        // Arrange
        int counterBefore = collectorService.getDataCount();
        LocalBroadcastManager local = LocalBroadcastManager.getInstance(this.getContext());

        // Act
        // Send a not matching action via broadcast
        String badData = "New instant message";
        Intent badIntent = new Intent(BleService.ACTION_DATA_AVAILABLE + "BAD!");
        // badIntent.putExtra(BleService.CHARACTERISTIC, badData);
        local.sendBroadcastSync(badIntent);

        // Assert
        int expectedCounterAfter = counterBefore; // No change should have been made
        int resultingCounterAfter = collectorService.getDataCount();
        assertEquals(expectedCounterAfter, resultingCounterAfter);
    }

    @Test
    public void testDataCountIncrement() {
        int countBefore = collectorService.getDataCount();
        GlucoseMeasurement measurement = mock(GlucoseMeasurement.class);
        collectorService.receiveMeasurement(measurement, "XXX");
        int expectedCountAfter = countBefore + 1;
        int resultedCountAfter = collectorService.getDataCount();
        assertEquals(expectedCountAfter, resultedCountAfter);
    }

    @Test
    public void testSetGetBleService() {
        // Arrange
        BleService initializedService = mock(BleService.class);
        when(initializedService.isProperlyInitialized()).thenReturn(true);
        BleService nonInitializedService = mock(BleService.class);
        when(nonInitializedService.isProperlyInitialized()).thenReturn(false);

        // Act
        collectorService.setBleService(null);
        BleService resultOnNull = collectorService.getBleService();
        collectorService.setBleService(initializedService);
        BleService resultOnInitialized = collectorService.getBleService();
        collectorService.setBleService(nonInitializedService);
        BleService resultOnNonInitialized = collectorService.getBleService();

        // Assert
        assertNull(resultOnNull);
        assertNull(resultOnNonInitialized);
        assertSame(initializedService, resultOnInitialized);
    }
}
