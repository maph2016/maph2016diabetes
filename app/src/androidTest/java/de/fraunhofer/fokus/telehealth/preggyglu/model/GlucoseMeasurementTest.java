package de.fraunhofer.fokus.telehealth.preggyglu.model;

import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by ogr on 05.09.2016.
 */
@RunWith(AndroidJUnit4.class)
public class GlucoseMeasurementTest extends AndroidTestCase {

    @Test
    public void testToInfluxLine() {
        // Arrange
        GlucoseMeasurement glucoseMock = mock(GlucoseMeasurement.class);
        when(glucoseMock.getSampleLocation()).thenReturn("Finger");
        when(glucoseMock.getType()).thenReturn("Capillary Whole Blood");
        when(glucoseMock.getTimeOffset()).thenReturn(0);
        when(glucoseMock.getUnit()).thenReturn("kg/L");
        // when(glucoseMock.getSequenceNumber()).thenReturn(0);
        when(glucoseMock.getGlucoseConcentration()).thenReturn(91.3f);

        when(glucoseMock.getBaseTime()).thenReturn(new Date(1473061580609l));

        when(glucoseMock.toInfluxLine()).thenCallRealMethod();

        // Act
        String result = glucoseMock.toInfluxLine();

        // Assert
        String expected = "Glucose,sample_location=Finger," +
                "type=Capillary\\ Whole\\ Blood,unit=kg/L glucose_concentration=91.3" +
                " 1473061580609000000";
        assertEquals(expected, result);
    }

}
