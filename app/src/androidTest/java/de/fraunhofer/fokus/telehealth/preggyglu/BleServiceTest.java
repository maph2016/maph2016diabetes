package de.fraunhofer.fokus.telehealth.preggyglu;

import android.content.Intent;
import android.os.IBinder;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import de.fraunhofer.fokus.telehealth.preggyglu.bluetooth.BleService;

/**
 * Created by ogr on 26.05.2016.
 */
@RunWith(AndroidJUnit4.class)
public class BleServiceTest extends AndroidTestCase {

    @ClassRule
    public static final ServiceTestRule mServiceRule = new ServiceTestRule();

    private static BleService service;

    @BeforeClass
    public static void bindService() throws TimeoutException {
        // Create service intent
        Intent serviceIntent = new Intent(
                InstrumentationRegistry.getTargetContext(), BleService.class);

        // Pass data to the service start
        serviceIntent.putExtra("start", "start");

        // Bind the service and grab a reference to the binder
        IBinder binder = mServiceRule.bindService(serviceIntent);

        // Get the reference to the started service
        // service = ((BleService.LocalBinder) binder).getService();
    }

    @Test
    public void dummy() {
        // TODO: Write some tests here
    }
}
