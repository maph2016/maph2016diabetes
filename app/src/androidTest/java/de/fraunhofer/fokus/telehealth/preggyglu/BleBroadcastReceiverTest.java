package de.fraunhofer.fokus.telehealth.preggyglu;

import android.content.IntentFilter;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.AndroidTestCase;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import de.fraunhofer.fokus.telehealth.preggyglu.bluetooth.BleBroadcastReceiver;
import de.fraunhofer.fokus.telehealth.preggyglu.bluetooth.BleService;
import de.fraunhofer.fokus.telehealth.preggyglu.collection.CollectorService;

import static org.mockito.Mockito.*;

/**
 * Created by ogr on 26.05.2016.
 */
@RunWith(AndroidJUnit4.class)
public class BleBroadcastReceiverTest extends AndroidTestCase {

    @ClassRule
    public static final ServiceTestRule mServiceRule = new ServiceTestRule();
    // the broadcast receiver works with two Services
    @Mock
    private BleService bleService; // Sender
    // data collectorService will only get mocked
    @Mock
    private CollectorService collectorService; // Receiver

    private BleBroadcastReceiver bcReceiver;
    private LocalBroadcastManager bcManager;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        bleService = mock(BleService.class);
        collectorService = mock(CollectorService.class);
        bcReceiver = new BleBroadcastReceiver(collectorService);

        // Register the broadcast-receiver
        bcManager = LocalBroadcastManager.getInstance(InstrumentationRegistry.getTargetContext());
        IntentFilter filter = bcReceiver.getIntentFilter();
        bcManager.registerReceiver(this.bcReceiver, filter);

    }

    @Ignore
    @Test
    public void testOnReceive_actionData() {

    }

}
