package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import de.fraunhofer.fokus.telehealth.preggyglu.collection.CollectorBroadcastReceiver;
import de.fraunhofer.fokus.telehealth.preggyglu.collection.CollectorService;
import de.fraunhofer.fokus.telehealth.preggyglu.collection.CollectorServiceConnection;
import de.fraunhofer.fokus.telehealth.preggyglu.model.DeviceInformation;
import de.fraunhofer.fokus.telehealth.preggyglu.model.Measurement;
import de.fraunhofer.fokus.telehealth.preggyglu.settings.SettingsBroadcastReceiver;
import de.fraunhofer.fokus.telehealth.preggyglu.upload.UploadBroadcastReceiver;

/**
 * Includes device bluetooth methods to get values
 * Created by Oliver on 18.01.2017.
 */
public abstract class RecordActivity extends Activity {

    private static String TAG = RecordActivity.class.getSimpleName();

    // Services the activity works with
    protected CollectorService collectorService;
    protected CollectorServiceConnection collectorServiceConnection;
    protected CollectorBroadcastReceiver collectorBroadcastReceiver;
    protected UploadBroadcastReceiver uploadBroadcastReceiver;
    protected SettingsBroadcastReceiver settingsBroadcastReceiver;
    protected LocalBroadcastManager localBroadcastManager;

    // current state of the collected data
    protected Map<Integer, Measurement> measurementMap;
    protected int currentCounter;
    protected Map<String, DeviceInformation> deviceInformationMap;

    public RecordActivity() {
        super();

        // services
        this.collectorService = null;
        this.measurementMap = Collections.synchronizedMap(new HashMap<Integer, Measurement>());
        this.currentCounter = 0;
        this.deviceInformationMap = Collections.synchronizedMap(new HashMap<String, DeviceInformation>());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.localBroadcastManager = LocalBroadcastManager.getInstance(this);

        // Register the BLE service and bind it
        Intent collectorServiceIntent = new Intent(this, CollectorService.class);
        this.collectorServiceConnection = new CollectorServiceConnection(this);
        bindService(collectorServiceIntent, collectorServiceConnection, BIND_AUTO_CREATE);

        // Load the preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register tbe broadcast listener, to get collector events
        this.collectorBroadcastReceiver = new CollectorBroadcastReceiver(this);
        IntentFilter collectorActions = collectorBroadcastReceiver.getIntentFilter();
        localBroadcastManager.registerReceiver(collectorBroadcastReceiver, collectorActions);

        // register broadcast listener for upload events
        this.uploadBroadcastReceiver = new UploadBroadcastReceiver(this);
        IntentFilter uploadActions = uploadBroadcastReceiver.getIntentFilter();
        localBroadcastManager.registerReceiver(uploadBroadcastReceiver, uploadActions);

        // register broadcast listener for settings messages
        this.settingsBroadcastReceiver = new SettingsBroadcastReceiver(this);
        IntentFilter settingsActions = settingsBroadcastReceiver.getIntentFilter();
        localBroadcastManager.registerReceiver(settingsBroadcastReceiver, settingsActions);

        // on first execution the collector service will not be bound
        if(this.collectorService != null) {
            refreshMeasurements();
            refreshDeviceInformation();
            refreshMessages();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        localBroadcastManager.unregisterReceiver(collectorBroadcastReceiver);
        localBroadcastManager.unregisterReceiver(uploadBroadcastReceiver);
        localBroadcastManager.unregisterReceiver(settingsBroadcastReceiver);
    }

    /**
     * registers the data collectorService service at the activity after connection.
     * Gets executed on creation of the service connection.
     *
     * @param collectorService
     */
    public void setCollectorService(CollectorService collectorService) {
        this.collectorService = collectorService;
        if (this.collectorService != null) {
            Log.d(TAG, "CollectorService successfully bound.");
            refreshMeasurements();
            refreshDeviceInformation();
            refreshMessages();
        } else {
            Log.d(TAG, "CollectorService service unbound.");
        }
    }

    /**
     * Triggers data update, so that the collector data from the collector service is grepped.
     */
    public void refreshMeasurements() {
        // Check connection
        if (this.collectorService == null) {
            Log.w(TAG, "Could not grep data from collector service, because it is still not connected.");
            return;
        }

        // Check revision
        if (collectorService.getDataCount() == this.currentCounter) {
            Log.d(TAG, "The current state \"" + this.currentCounter + "\" is up-to-date." +
                    "No data will be grepped.");
            return;
        }

        // When the versions differ receive the new data.
        Map<Integer, Measurement> there = collectorService.getMeasurementMap();
        Map<Integer, Measurement> here = this.measurementMap;

        // Search for new entries and collect them
        for (Integer id : there.keySet()) {
            if (!here.containsKey(id)) {
                Measurement newMeasurement = there.get(id);
                here.put(id, newMeasurement);
                onMeasurementReceived(newMeasurement);
            }
        }

        // update the current revision
        this.currentCounter = collectorService.getDataCount();
    }

    public void refreshDeviceInformation() {
        Map<String, DeviceInformation> there = collectorService.getDeviceInformationMap();
        Map<String, DeviceInformation> here = this.deviceInformationMap;

        for (String deviceAddress : there.keySet()) {
            if (!here.containsKey(deviceAddress)) {
                DeviceInformation newInfo = there.get(deviceAddress);
                here.put(deviceAddress, newInfo);
            }
        }

        // showDeviceInformation();
    }

    // protected abstract void showDeviceInformation();

    protected abstract void onMeasurementReceived(Measurement measurement);

    public void listMessage(final String message) {
        Log.d(TAG, message);
    }

    /**
     * Polls all new messages from the collector service.
     */
    public void refreshMessages() {
        Queue<String> queue = collectorService.getMessageQueue();
        while (queue.peek() != null) {
            Log.d(TAG, queue.poll());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(this.collectorServiceConnection);
    }
}
