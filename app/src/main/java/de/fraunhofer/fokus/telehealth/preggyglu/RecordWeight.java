package de.fraunhofer.fokus.telehealth.preggyglu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import de.fraunhofer.fokus.telehealth.preggyglu.model.GlucoseMeasurement;
import de.fraunhofer.fokus.telehealth.preggyglu.model.Measurement;
import de.fraunhofer.fokus.telehealth.preggyglu.model.WeightMeasurement;
import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.WeightRecordModel;
import io.realm.Realm;

/**
 * Created by Maria on 14.12.2016.
 */
public class RecordWeight extends RecordActivity
{
    private WeightRecordModel record;
    private Button continueButton;
    private Realm realm;
    private EditText weightField;

    @Override
    protected void onMeasurementReceived(Measurement measurement) {
        // Check type
        if(measurement.getClass() != WeightMeasurement.class) {
            return;
        }
        WeightMeasurement weightMeasurement = (WeightMeasurement) measurement;

        // Show info
        Toast toast = Toast.makeText(
                getApplicationContext(),
                "New Weight Measurement received!",
                Toast.LENGTH_SHORT);
        toast.show();

        // Display value
        double weight = weightMeasurement.getWeight();
        weightField.setText(String.valueOf(weight));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_weight);
        weightField = (EditText) findViewById(R.id.weightValue);

        // Initialize Realm
        Realm.init(this);

        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();

        continueButton = (Button) findViewById(R.id.btnContinue);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeCurrentValue();
                Intent i = new Intent(RecordWeight.this, Analysis.class);
                startActivity(i);
            }
        });
    }

    private void storeCurrentValue() {
        WeightRecordModel record = new WeightRecordModel();
        record.setDate(Calendar.getInstance().getTime());

        try {
            double input = Double.parseDouble(weightField.getText().toString());
            record.setValue(input);
        } catch(NumberFormatException nfe) {
            return;
        }

        // Persist your data in a transactions
        realm.beginTransaction();
        realm.copyToRealm(record); // Persist unmanaged objects
        realm.commitTransaction();
    }

}
