package de.fraunhofer.fokus.telehealth.preggyglu.realmmodels;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by Pepe on 04.01.2017.
 */

public class WeightRecordModel extends RealmObject {
    private double value;
    private Date date;

    public Date getDate() {
        return date;
    }
    public double getValue() {
        return value;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public void setValue(double value) {
        this.value = value;
    }
}
