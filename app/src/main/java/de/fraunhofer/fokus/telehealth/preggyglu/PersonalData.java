package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.PersonalDataModel;
import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;
import io.realm.Sort;

import android.app.DatePickerDialog;
import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;

import org.w3c.dom.Text;

/**
 * Created by Maria on 14.12.2016.
 */
public class PersonalData  extends FragmentActivity implements RealmModel {
    private Button continueButton;
    private PersonalDataModel data;
    private Realm realm;
    private static TextView birthdateTextView;
    private static TextView calculatedBirthdateTextView;
    private TextView weightTextView;
    private TextView bodysizeTextView;
    private TextView familySpinnerLabel;
    private CheckBox familyCheckBox;
    private Spinner familySpinner;
    private boolean firstStart;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_data);

        // Initialize Realm
        Realm.init(this);

        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();

        firstStart = true;

        birthdateTextView = (TextView) findViewById(R.id.inputBirthdate);
        calculatedBirthdateTextView = (TextView) findViewById(R.id.inputCalculatedBirthdate);
        weightTextView = (TextView) findViewById(R.id.inputWeight);
        bodysizeTextView = (TextView) findViewById(R.id.inputBodysize);
        familyCheckBox = (CheckBox) findViewById(R.id.inputFamilyCheckbox);
        familySpinner = (Spinner) findViewById(R.id.inputFamilySpinner);
        familySpinnerLabel = (TextView) findViewById(R.id.InputFamilySpinnerLabel);

        RealmResults<PersonalDataModel> all = realm.where(PersonalDataModel.class).findAll();

        if (all.size() > 0) {
            firstStart = false;
            PersonalDataModel oldData = all.last();
            birthdateTextView.setText(oldData.getBirthdate());
            calculatedBirthdateTextView.setText(oldData.getCalculatedBirthdate());
            weightTextView.setText(Integer.toString(oldData.getWeight()));
            bodysizeTextView.setText(Integer.toString(oldData.getBodysize()));
            familyCheckBox.setChecked(oldData.isDiabetesInFamily());

            if (familyCheckBox.isChecked()) {
                familySpinnerLabel.setVisibility(View.VISIBLE);
                familySpinner.setVisibility(View.VISIBLE);
            }
            int index = 0;
            for (int i=0;i<familySpinner.getCount();i++){
                if (familySpinner.getItemAtPosition(i).toString().equalsIgnoreCase(oldData.getDiabeticPerson())){
                    index = i;
                    break;
                }
            }
            familySpinner.setSelection(index);

        }

        familyCheckBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if (familyCheckBox.isChecked())
                {
                    familySpinner.setVisibility(View.VISIBLE);
                    familySpinnerLabel.setVisibility(View.VISIBLE);
                }
                else
                {
                    familySpinner.setVisibility(View.GONE);
                    familySpinnerLabel.setVisibility(View.GONE);
                }

            }
        });

        continueButton = (Button) findViewById(R.id.btnContinue);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data = new PersonalDataModel();

                data.setBirthdate(birthdateTextView.getText().toString());
                data.setCalculatedBirthdate(calculatedBirthdateTextView.getText().toString());
                data.setBodysize(Integer.parseInt(bodysizeTextView.getText().toString()));
                data.setWeight(Integer.parseInt(weightTextView.getText().toString()));
                data.setDiabetesInFamily(familyCheckBox.isChecked());
                data.setDiabeticPerson(String.valueOf(familySpinner.getSelectedItem()));

                // Persist your data in a transaction
                realm.beginTransaction();
                realm.copyToRealm(data); // Persist unmanaged objects
                realm.commitTransaction();

                RealmResults<PersonalDataModel> all = realm.where(PersonalDataModel.class).findAll();
                Intent i;
                if (firstStart)
                    i = new Intent(PersonalData.this, RecordGlucose.class);
                else
                    i = new Intent(PersonalData.this, MainMenu.class);
                startActivity(i);
            }
        });

       birthdateTextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogFragment birthdatePicker = new DatePickerFragment();
                birthdatePicker.show(getSupportFragmentManager(), "timePicker");
            }
        });

        calculatedBirthdateTextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogFragment calculatedBirthdatePicker = new DatePickerFragment2();
                calculatedBirthdatePicker.show(getSupportFragmentManager(), "timePicker");
            }
        });

    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            birthdateTextView.setText(new StringBuilder().append(day).append(".")
                    .append(month+1).append(".").append(year));
        }
    }

    public static class DatePickerFragment2 extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            calculatedBirthdateTextView.setText(new StringBuilder().append(day).append(".")
                    .append(month+1).append(".").append(year));
        }
    }
}
