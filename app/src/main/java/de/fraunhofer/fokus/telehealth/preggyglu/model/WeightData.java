package de.fraunhofer.fokus.telehealth.preggyglu.model;

/**
 * Created by Aijana on 15.12.2016.
 * POJO for weight view
 */

public class WeightData extends Data{
    private WeightMeasurement glucoseValues;


    public WeightMeasurement getGlucoseValues() {
        return glucoseValues;
    }

    public void setGlucoseValues(WeightMeasurement glucoseValues) {
        this.glucoseValues = glucoseValues;
    }
}
