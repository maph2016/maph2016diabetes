package de.fraunhofer.fokus.telehealth.preggyglu.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import de.fraunhofer.fokus.telehealth.preggyglu.RecordActivity;
import de.fraunhofer.fokus.telehealth.preggyglu.collection.CollectorServicePreferenceListener;

/**
 * The main activity's broadcast receiver, which listens for news of the settings activity.
 */
public class SettingsBroadcastReceiver extends BroadcastReceiver {

    // actions this broadcast receiver is made for
    public final String[] ACTIONS = {
            CollectorServicePreferenceListener.SETTINGS_EVENT
    };

    // Tag for logging purposes
    private final String TAG = SettingsBroadcastReceiver.class.getSimpleName();

    // the main activity to which the data will be forwarded
    private RecordActivity recordActivity;

    public SettingsBroadcastReceiver(RecordActivity recordActivity) {
        this.recordActivity = recordActivity;
    }

    /**
     * Create an intent filter this receiver works with.
     *
     * @return
     */
    public IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        for (String action : ACTIONS) {
            filter.addAction(action);
        }
        return filter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action == CollectorServicePreferenceListener.SETTINGS_EVENT) {
            recordActivity.listMessage(intent.getStringExtra(
                    CollectorServicePreferenceListener.MESSAGE));
        }
    }
}
