package de.fraunhofer.fokus.telehealth.preggyglu.realmmodels;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by Pepe on 04.01.2017.
 */

public class PersonalDataModel extends RealmObject {
    private String birthdate;
    private String calculatedBirthdate;
    private int weight;
    private int bodysize;
    private boolean diabetesInFamily;
    private String diabeticPerson;

    public String getBirthdate()
    {
        return birthdate;
    }

    public void setBirthdate(String birthdate)
    {
        this.birthdate = birthdate;
    }

    public String getCalculatedBirthdate()
    {
        return calculatedBirthdate;
    }

    public void setCalculatedBirthdate(String calculatedBirthdate)
    {
        this.calculatedBirthdate = calculatedBirthdate;
    }

    public int getWeight()
    {
        return weight;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    public int getBodysize()
    {
        return bodysize;
    }

    public void setBodysize(int bodysize)
    {
        this.bodysize = bodysize;
    }

    public boolean isDiabetesInFamily()
    {
        return diabetesInFamily;
    }

    public void setDiabetesInFamily(boolean diabetesInFamily)
    {
        this.diabetesInFamily = diabetesInFamily;
    }

    public String getDiabeticPerson()
    {
        return diabeticPerson;
    }

    public void setDiabeticPerson(String diabeticPerson)
    {
        this.diabeticPerson = diabeticPerson;
    }
}
