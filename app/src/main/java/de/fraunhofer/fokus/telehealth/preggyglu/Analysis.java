package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.GlucoseRecordModel;
import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.PersonalDataModel;
import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.WeightRecordModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maria on 14.12.2016.
 */
public class Analysis extends Activity
{
    private Button continueButton;
    private TextView topTextView;
    private TextView descriptionTextView;
    private TextView smileyTextView;
    private TextView downTextView;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.analysis);
        topTextView = (TextView) findViewById(R.id.textViewTop);
        descriptionTextView = (TextView) findViewById(R.id.textViewDescription);
        smileyTextView = (TextView) findViewById(R.id.textViewSmiley);
        downTextView = (TextView) findViewById(R.id.textViewDown);
        continueButton = (Button) findViewById(R.id.btnContinue);

        boolean diabetesInFamily = false;
        boolean overweight = false;
        boolean weightIncrease = false;
        boolean highDiabeticValue = false;
        boolean highAge = false;

        continueButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(Analysis.this, MainMenu.class);
                startActivity(i);
            }
        });


        // Initialize Realm
        Realm.init(this);

        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();

        RealmResults<PersonalDataModel> allPersonalData = realm.where(PersonalDataModel.class).findAll();
        PersonalDataModel personalData;

        RealmResults<WeightRecordModel> allWeightRecords = realm.where(WeightRecordModel.class).findAll();
        WeightRecordModel weightRecord;

        RealmResults<GlucoseRecordModel> allGlucoseRecords = realm.where(GlucoseRecordModel.class).findAll();
        GlucoseRecordModel glucoseRecord;

        int weight = 0;
        int bodySize = 0;
        double bmi = 0;

        if (allPersonalData.size() > 0)
        {
            personalData = allPersonalData.last();
            if (personalData.isDiabetesInFamily() && !personalData.getDiabeticPerson().equals("Andere"))
                diabetesInFamily = true;

            String birthdate = personalData.getBirthdate();
            int birthyear = Integer.parseInt(birthdate.split("\\.")[2]);
            if (birthyear < 1987)
                highAge = true;

            bodySize = personalData.getBodysize();
            weight = personalData.getWeight();
            bmi = weight / (bodySize * bodySize);
            if (bmi > 25)
                overweight = true;
        }
        if (allWeightRecords.size() > 0) {
            weightRecord = allWeightRecords.last();

            if (weightRecord.getValue() > (weight + 16))
                weightIncrease = true;
        }

        if (allGlucoseRecords.size() > 0) {
            glucoseRecord = allGlucoseRecords.last();
            if (glucoseRecord.getValue() > 92)
                highDiabeticValue = true;
        }

        if (highDiabeticValue && weightIncrease)
            topTextView.setText("Dein Glukosewert ist erhöht und Dein Gewicht hat überdurchschnittlich zugenommen.");
        else if (highDiabeticValue)
            topTextView.setText("Dein Glukosewert ist erhöht.");
        else if (weightIncrease)
            topTextView.setText("Dein Gewicht hat überdurchschnittlich zugenommen.");
        else
            topTextView.setText("Deine Werte sind derzeit vollkommen unbedenklich.");

        if (diabetesInFamily && overweight && highAge)
            descriptionTextView.setText("Aufgrund Deiner familiären Vorbelastung, Deines erhöhten BMIs und Deines Alters " +
                    "gehörst Du zur Risikogruppe für Schwangerschaftsdiabetes.");
        else if (diabetesInFamily && overweight)
            descriptionTextView.setText("Aufgrund Deiner familiären Vorbelastung und Deines erhöhten BMIs " +
                                 "gehörst du zur Risikogruppe für Schwangerschaftsdiabetes.");
        else if (diabetesInFamily && highAge)
            descriptionTextView.setText("Aufgrund Deiner familiären Vorbelastung und Deines Alters " +
                "gehörst Du zur Risikogruppe für Schwangerschaftsdiabetes.");
        else if (highAge && overweight)
            descriptionTextView.setText("Aufgrund Deines Alters und Deines erhöhten BMIs " +
                    "gehörst Du zur Risikogruppe für Schwangerschaftsdiabetes.");
        else if (diabetesInFamily)
            descriptionTextView.setText("Aufgrund Deiner familiären Vorbelastung " +
                                        "gehörst Du zur Risikogruppe für Schwangerschaftsdiabetes.");
        else if (overweight)
            descriptionTextView.setText("Aufgrund Deines erhöhten BMIs " +
                    "gehörst Du zur Risikogruppe für Schwangerschaftsdiabetes.");
        else if (highAge)
            descriptionTextView.setText("Aufgrund Deines Alters " +
                    "gehörst Du zur Risikogruppe für Schwangerschaftsdiabetes.");
        else
        {
            descriptionTextView.setText("Du gehörst nicht zur Risikogruppe für Schwangerschaftsdiabetes.");
            downTextView.setText("Mit wöchentlichen Tests kannst Du Deine Werte trotzdem protokollieren.");
        }

        if (diabetesInFamily || overweight)
            downTextView.setText("Wir empfehlen den Test wöchentlich zu wiederholen.");

        if (highDiabeticValue || weightIncrease)
        {
            smileyTextView.setText("\uD83D\uDE10");
            descriptionTextView.setText("Bitte suche einen Arzt auf und lass Deine Werte überprüfen.");
            downTextView.setText("");
        }
        else
        {
            smileyTextView.setText("\uD83D\uDE0A");
        }

    }
}
