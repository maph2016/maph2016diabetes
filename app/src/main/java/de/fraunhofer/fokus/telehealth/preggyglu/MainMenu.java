package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Maria on 14.12.2016.
 */
public class MainMenu extends Activity
{
    private Button glucoseButton;
    private Button weightButton;
    private Button tipsButton;
    private Button personalDataButton;
    private Button dataProtectionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        glucoseButton = (Button) findViewById(R.id.btnGlucose);
        weightButton = (Button) findViewById(R.id.btnWeight);
        tipsButton = (Button) findViewById(R.id.btnTips);
        personalDataButton = (Button) findViewById(R.id.btnPersonalData);
        dataProtectionButton = (Button) findViewById(R.id.btnDataProtection);

        glucoseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMenu.this, Glucose.class);
                startActivity(i);
            }
        });

        weightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMenu.this, Weight.class);
                startActivity(i);
            }
        });

        tipsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMenu.this, Tips.class);
                startActivity(i);
            }
        });

        personalDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMenu.this, PersonalData.class);
                startActivity(i);
            }
        });

        dataProtectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMenu.this, DataProtection.class);
                startActivity(i);
            }
        });
    }
}
