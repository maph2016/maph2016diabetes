package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.PersonalDataModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maria on 14.12.2016.
 */
public class Welcome extends Activity
{
    private Button continueButton;
    private boolean firstStart;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

        // Initialize Realm
        Realm.init(this);

        // Get a Realm instance for this thread
        Realm realm = Realm.getDefaultInstance();

        final RealmResults<PersonalDataModel> data = realm.where(PersonalDataModel.class).findAll();
        firstStart = (data.size() == 0) ? true : false;

        continueButton = (Button) findViewById(R.id.btnContinue);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                if (firstStart)
                    i = new Intent(Welcome.this, PersonalData.class);
                else
                    i = new Intent(Welcome.this, MainMenu.class);
                startActivity(i);
            }
        });
    }
}
