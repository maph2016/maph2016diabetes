package de.fraunhofer.fokus.telehealth.preggyglu.collection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.icu.text.AlphabeticIndex;

import de.fraunhofer.fokus.telehealth.preggyglu.RecordActivity;

/**
 * The main activity's broadcast receiver, which listens for news by the collector
 * service.
 */
public class CollectorBroadcastReceiver extends BroadcastReceiver {

    // actions this broadcast receiver is made for
    public final String[] ACTIONS = {
            CollectorService.ACTION_MEASUREMENT_COLLECTED,
            CollectorService.ACTION_DEVICE_INFO_COLLECTED,
            CollectorService.COLLECTOR_STOPPED,
            CollectorService.NEW_MESSAGES
    };

    // Tag for logging purposes
    private final String TAG = CollectorBroadcastReceiver.class.getSimpleName();

    // the main activity to which the data will be forwarded
    private RecordActivity recordActivity;

    public CollectorBroadcastReceiver(RecordActivity recordActivity)  {
        this.recordActivity = recordActivity;
    }

    /**
     * Create an intent filter this receiver works with.
     * @return
     */
    public IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        for(String action : ACTIONS) {
            filter.addAction(action);
        }
        return filter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        switch(action) {
            case CollectorService.ACTION_MEASUREMENT_COLLECTED:
                recordActivity.refreshMeasurements();
                break;
            case CollectorService.ACTION_DEVICE_INFO_COLLECTED:
                recordActivity.refreshDeviceInformation();
                break;
            case CollectorService.COLLECTOR_STOPPED:
                break;
            case CollectorService.NEW_MESSAGES:
                recordActivity.refreshMessages();
                break;
            default:
                break;
        }

    }
}
