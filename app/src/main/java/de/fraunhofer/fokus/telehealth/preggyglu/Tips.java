package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Maria on 14.12.2016.
 */
public class Tips extends Activity
{
    private Button nextTipButton;
    private TextView tip;
    private Button backButton;
    private String[] tippsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tips);

        nextTipButton = (Button) findViewById(R.id.btnContinue);
        tip= (TextView) findViewById(R.id.tipText);
        backButton=(Button) findViewById(R.id.backButton);
        tippsArray=getSomeTipps();

        tip.setText("Wusstest Du schon, dass Du während der Schwangerschaft keinen Fisch essen solltest?");


        nextTipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    tip.setText(selectATip(tippsArray));
            }});

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tips.this, MainMenu.class);
                startActivity(i);
            }
        });
    }

    private String[] getSomeTipps(){

        String[] tipps= new String[22];
        tipps[0]="Wusstest Du schon, dass Du während der Schwangerschaft keinen Fisch essen solltest?";
        tipps[1]="Wusstest Du schon, ausreichend Bewegung tut Dir und Deinem Kind gut?";
        tipps[2]="Wusstest Du schon, dass es in der Schwangerschaft ist es wichtig ausreichend zu Trinken. 1,5 Liter oder mehr solltest Du täglich an Flüssigkeit in Form von Getränken zu sich nehmen?";
        tipps[3]="Wusstest Du schon, dass das eine Frau während einer normalen Schwangerschaft etwa 10 bis 16 Kilogramm zu nimmt?";
        tipps[4]="Wusstest Du schon, dass Milch, Joghurt und Käse liefern wertvolles Kalzium? Auf Rohmilchprodukte sollten Schwangere jedoch verzichten.";
		tipps[5]="Wusstest Du schon, dass Du das Risiko einer Entstehung von Schwangerschaftsdiabetes deutlich reduzierst, wenn Du Übergewicht vermeidest und auf Deine Ernährung achtest?";
		tipps[6]="Wusstest Du schon, dass gerade während einer Schwangerschaft eine ausgewogene Ernährung mit viel Obst und Gemüse, Vollkornprodukten sowie wenig fett- und zuckerreichen Lebensmitteln wichtig ist?";
		tipps[7]="Wusstest Du schon, dass Du bei Obst und Gemüse: Auf frische Ware achten und vor dem Verzehr gründlich waschen sollst?";
		tipps[8]="Wusstest Du schon, dass Fleisch wertvolles Eisen und Eiweiß liefert? Mit fetter Wurst sollten Schwangere jedoch sparsam umgehen.";
		tipps[9]="Wusstest Du schon, dass rodukte tabu sind, die roh, nicht ausreichend erhitzt oder nur kaltgeräuchert sind? Da diese Lebensmittel mit gefährlichen Keimen kontaminiert sein können.";
		tipps[10]="Wusstest Du schon, dass auf Kaffee in der Schwangerschaft in der Regel nicht verzichtet werden muss? Erlaubt sind täglich etwa 300 Milligramm, was ein bis zwei Tassen entspricht.";
		tipps[11]="Wusstest Du schon, dass das Baby in der 20. Schwangerschaftswoche rund 400 Milliliter Fruchtwasser am Tag trinkt?";
		tipps[12]="Wusstest Du schon, dass eine Devise lautet für zwei Denken, aber nicht für zwei Essen? Eine ausgewogene Ernährung ist dabei wichtig.";
		tipps[13]="Wusstest Du schon, dass bereits ab der 15. bis 18. Woche, das Baby erste Geräusche im Körper der Mutter wahrnimmt.?";
		tipps[14]="Wusstest Du schon, dass Geräusche von außen wie Stimmen oder Spieluhren kann das Kind ab der 23. bis 24. Schwangerschaftswoche hören?";
		tipps[15]="Wusstest Du schon, dass in der 10. Schwangerschaftswoche (SSW) ist das Baby erst 25 Millimeter groß, aber schon fast vollständig geformt, vom Herz über das Gehirn bis hin zu den Gliedmaßen?";
		tipps[16]="Wusstest Du schon, dass bereits ab der 13. SSW kann das Baby helles Licht von Dunkelheit unterscheiden? Es reagiert, wenn man eine Taschenlampe direkt auf den Bauch scheinen lässt. ";
        tipps[17]="Wusstest Du schon, dass hinter Gelüsten manchmal mehr steckt? Ständige Lust auf Schokolade kann z.B. ein Magnesiummangel sein, oder Appetit auf Erde ein Eisenmangel.";
		tipps[18]="Wusstest Du schon, dass Du eine langsamere Verdauung hast? Durch den Einfluss von Progesteron wird die Verdauung verlangsamt, so können mehr Nährstoffe für Dein Baby rausgefiltert werden.";
		tipps[19]="Wusstest Du schon, dass Dein Geruchssinn verstärkt wird? Häufig ein früher Hinweis auf eine Schwangerschaft. Veränderungen der Essensvorlieben oder -Abneigungen sind vor allem mit dem Geruchssinn und weniger dem Geschmacksinn verbunden.";
		tipps[20]="Wusstest Du schon, dass im Verlauf der Schwangerschaft die Blutflüssigkeit der Mutter um bis zu 50 Prozent ansteigt, um unter anderem die Durchblutung der Gebärmutter zu gewährleisten?";
		tipps[21]="Wusstest Du schon, dass ab der 17. Woche sollten Schwangere ein Plus von etwa 250 Kalorien am Tag zu sich nehmen sollen? Das entspricht beispielsweise 150 Gramm Vollmilchjoghurt plus einen großen Apfel plus etwa hundert Gramm Schollenfilet.";
		return tipps;
    }

    private String selectATip(String[] allTipps){

        Random r = new Random();
        return allTipps[r.nextInt(allTipps.length-1)];
    }
}
