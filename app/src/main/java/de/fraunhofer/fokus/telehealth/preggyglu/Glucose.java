package de.fraunhofer.fokus.telehealth.preggyglu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Date;

import de.fraunhofer.fokus.telehealth.preggyglu.model.Data;
import de.fraunhofer.fokus.telehealth.preggyglu.model.GlucoseData;
import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.GlucoseRecordModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maria on 14.12.2016.
 * Overview of all glucose measurements
 * Edited by Aijana on 15.12.2016
 */
public class Glucose  extends Diagram
{

    private Button continueButton;
    private ArrayList<Data> dummyData;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        dummyData=getData();
        setContentView(R.layout.glucose);
        createDiagramm(dummyData,getControllData(dummyData),R.id.glucoseChart, "Glukose Messungen", "Glukose in mg/dL", 0,140);

        continueButton = (Button) findViewById(R.id.btnContinue);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Glucose.this, RecordGlucose.class);
                startActivity(i);
            }
        });
    }

     ArrayList<Data> getData(){

         // Initialize Realm
         Realm.init(this);

         // Get a Realm instance for this thread
         realm = Realm.getDefaultInstance();

         //get all glucoserecord data from db
         RealmResults<GlucoseRecordModel> results = realm.where(GlucoseRecordModel.class).findAll();


        ArrayList<Data> data = new ArrayList<Data>();

         //add db results to data list
         for(GlucoseRecordModel record : results){
             GlucoseData d = new GlucoseData();
             d.setDate(record.getDate());
             d.setValue(record.getValue());
             data.add(d);
         }
         
        return data;
    }

    @Override
    ArrayList<Data> getControllData(ArrayList<Data> data) {

        ArrayList<Data> controllData= new ArrayList<Data>();
        for(Data d : data){
            GlucoseData glucoseData= new GlucoseData();
            glucoseData.setValue(92);
            glucoseData.setDate(d.getDate());
            controllData.add(glucoseData);
        }

        return controllData;
    }


}
