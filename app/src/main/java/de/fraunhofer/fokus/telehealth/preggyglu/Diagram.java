package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.LinearLayout;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.Date;

import de.fraunhofer.fokus.telehealth.preggyglu.model.Data;

/**
 * Created by Aijana on 15.12.2016.
 * Base Activity for diagram creation
 */

public abstract class Diagram extends Activity {

    protected void createDiagramm(ArrayList<Data> data, ArrayList<Data> controllData, int viewid, String seriesTitle, String valuedataTitle, int minY, int maxY) {

        //diagramm data
        XYSeries series = new XYSeries(seriesTitle);

        if(data.size()>0){
            for(int i=0; i<data.size();i++)
            {
                 series.add(data.get(i).getDate().getTime(),data.get(i).getValue());
            }
        }

        XYSeries controllSeries = new XYSeries("Richtwert");

        if(controllData!= null && controllData.size()>0){
            for(int i=0; i<controllData.size();i++)
            {
                controllSeries.add(controllData.get(i).getDate().getTime(),controllData.get(i).getValue());
            }
        }

        //add data to dataset
        XYMultipleSeriesDataset xyMultipleSeriesDataset = new XYMultipleSeriesDataset();
        xyMultipleSeriesDataset.addSeries(series);
        if(controllData!= null && controllData.size()>0)  xyMultipleSeriesDataset.addSeries(controllSeries);

        //renderer for data
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setLineWidth(5);
        renderer.setColor(Color.RED);
        //add point markers
        renderer.setPointStyle(PointStyle.CIRCLE);
        renderer.setPointStrokeWidth(6);


        //renderer for controlldata
        XYSeriesRenderer controlldataRenderer = new XYSeriesRenderer();
        controlldataRenderer.setLineWidth(5);
        controlldataRenderer.setColor(Color.argb(150,0,102,51));
        //add point markers
        controlldataRenderer.setPointStyle(PointStyle.CIRCLE);
        controlldataRenderer.setPointStrokeWidth(6);

        //costumize chart (renderer)
        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
        mRenderer.addSeriesRenderer(renderer);
        if(controllData!= null && controllData.size()>0) mRenderer.addSeriesRenderer(controlldataRenderer);

        //  avoid black border around chart => transparent margins
        mRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));

        //enable pan on x axis
        mRenderer.setPanEnabled(true, false);
        //restrict pan to avoid infinite x axis
        double THREEDAYS = 81300000 *3;

        if(data.size()>0)
            mRenderer.setPanLimits(new double[] { data.get(0).getDate().getTime(), data.get((data.size()-1)).getDate().getTime()+THREEDAYS, 0, 1000});

        //set min/max of Y axis
        mRenderer.setYAxisMax(maxY);
        mRenderer.setYAxisMin(minY);

        //set (start) centering for x values
        if(data.size()>3){
            mRenderer.setXAxisMin(data.get(data.size()-3).getDate().getTime());
            mRenderer.setXAxisMin(data.get((data.size()-1)).getDate().getTime());
        }

        mRenderer.setShowGrid(true);

        //costumize title
        mRenderer.setXTitle("Datum");
        mRenderer.setYTitle(valuedataTitle);
        mRenderer.setLabelsColor(Color.BLACK);
        mRenderer.setAxisTitleTextSize(50);

        //set axes styles and costumize labels
        mRenderer.setAxesColor(Color.BLACK);
        mRenderer.setXLabelsColor(Color.BLACK);
        mRenderer.setYLabelsColor(0, Color.BLACK);
        mRenderer.setLabelsTextSize(35);
        mRenderer.setLegendTextSize(30);

        mRenderer.setYLabelsAlign(Paint.Align.LEFT);
        mRenderer.setXLabelsAlign(Paint.Align.CENTER);
        mRenderer.setLegendHeight(90);


        //if no controll data is available hide series legend
        if(controllData== null) mRenderer.setShowLegend(false);
        //adjust margins accordingly (else legends overlapps with title of x axis)
        if(mRenderer.isShowLegend()) mRenderer.setMargins(new int[] { 20, 50,90, 20 });
        else mRenderer.setMargins(new int[] { 20, 50,10, 20 });

        //add diagramm to view
        View chart = ChartFactory.getTimeChartView(getBaseContext(), xyMultipleSeriesDataset, mRenderer,"dd-MMM-yy");
        LinearLayout chartContainer = (LinearLayout) findViewById(viewid);
        chartContainer.addView(chart);
    }

    abstract ArrayList<Data> getData();
    abstract ArrayList<Data> getControllData(ArrayList<Data> data);
}
