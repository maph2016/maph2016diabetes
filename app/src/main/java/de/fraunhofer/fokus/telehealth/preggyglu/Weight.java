package de.fraunhofer.fokus.telehealth.preggyglu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Date;

import de.fraunhofer.fokus.telehealth.preggyglu.model.Data;
import de.fraunhofer.fokus.telehealth.preggyglu.model.WeightData;
import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.GlucoseRecordModel;
import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.WeightRecordModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maria on 14.12.2016.
 */
public class Weight extends Diagram
{
    private Button continueButton;
    private ArrayList<Data> dummyData;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        dummyData=getData();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weight);
        createDiagramm(dummyData,getControllData(dummyData), R.id.weightChart, "Gewichts Messungen", "Gewicht in kg",40,130);
        continueButton = (Button) findViewById(R.id.btnContinue);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Weight.this, RecordWeight.class);
                startActivity(i);
            }
        });
    }

     ArrayList<Data> getData(){

         // Initialize Realm
         Realm.init(this);

         // Get a Realm instance for this thread
         realm = Realm.getDefaultInstance();

         //get all weightrecord data from db
         RealmResults<WeightRecordModel> results = realm.where(WeightRecordModel.class).findAll();


         ArrayList<Data> data = new ArrayList<Data>();

         //add db results to data list
         for(WeightRecordModel record : results){
             WeightData d = new WeightData();
             d.setDate(record.getDate());
             d.setValue(record.getValue());
             data.add(d);
         }


        return data;
    }

    @Override
    ArrayList<Data> getControllData(ArrayList<Data> dummyData) {
        return null;
    }

}
