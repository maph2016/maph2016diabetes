package de.fraunhofer.fokus.telehealth.preggyglu.model;

/**
 * Created by Aijana on 15.12.2016.
 * POJO for diagramm (Glucose view)
 */

public class GlucoseData extends Data{


    private GlucoseMeasurement glucoseValues;

    public GlucoseMeasurement getGlucoseValues() {
        return glucoseValues;
    }

    public void setGlucoseValues(GlucoseMeasurement glucoseValues) {
        this.glucoseValues = glucoseValues;
    }
}
