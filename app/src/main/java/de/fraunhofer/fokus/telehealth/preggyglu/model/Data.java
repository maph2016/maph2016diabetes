package de.fraunhofer.fokus.telehealth.preggyglu.model;

import java.util.Date;

/**
 * Created by Aijana on 15.12.2016.
 * Base class
 */

public class Data {

    private double value;
    private Date date;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
