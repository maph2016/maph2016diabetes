package de.fraunhofer.fokus.telehealth.preggyglu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DataProtection extends Activity {


    private Button backButton;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_protection);
        backButton= (Button) findViewById(R.id.backBtn);
        text = (TextView) findViewById(R.id.dataProtectionId);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DataProtection.this,MainMenu.class);
                startActivity(i);
            }
        });

        text.setText("Die Betreiber von PreggyGlu nehmen den Schutz Ihrer persönlichen Daten sehr ernst. \n" +
                "Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung. " +
                "Die Nutzung von PreggyGlu ist auch ohne Angabe von personenbezogenen Daten möglich, dabei können aber nicht alle Funktionalitäten voll genutzt werden. " +
                "Ihre Daten werden lokal auf Ihrem Smartphone gespeichert und nicht auf externen Servern. Sie können Ihre Daten jederzeit löschen.\n\n" +
                "Folgende Daten werden benötigt, um Ihre Schwangerschafts-Diabetes-Gefährdung richtig zu berechnen: \n\n"+
                "- Geburtsdatum\n" +
                "- Berechneter Geburtstermin\n" +
                "- Gewicht vor der Schwangerschaft\n" +
                "- Diabetes Vorbelastung in der Familie \n\n" +
                "Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich."
        );
    }



}
