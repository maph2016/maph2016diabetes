package de.fraunhofer.fokus.telehealth.preggyglu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import de.fraunhofer.fokus.telehealth.preggyglu.model.GlucoseMeasurement;
import de.fraunhofer.fokus.telehealth.preggyglu.model.Measurement;
import de.fraunhofer.fokus.telehealth.preggyglu.realmmodels.GlucoseRecordModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maria on 14.12.2016.
 */
public class RecordGlucose extends RecordActivity {
    private Button continueButton;
    private EditText glucoseField;
    private Realm realm;

    @Override
    protected void onMeasurementReceived(Measurement measurement) {
        // Check type
        if (measurement.getClass() != GlucoseMeasurement.class) {
            return;
        }
        GlucoseMeasurement glucoseMeasurement = (GlucoseMeasurement) measurement;

        // Show info
        Toast toast = Toast.makeText(
                getApplicationContext(),
                "New Glucose Measurement received!",
                Toast.LENGTH_SHORT);
        toast.show();

        // Convert value to correct unit
        float concentration = glucoseMeasurement.getGlucoseConcentration();

        // Display value
        DecimalFormat df = new DecimalFormat("#.#");
        glucoseField.setText(df.format(concentration * 100000));

    }

    private void storeCurrentValue() {
        GlucoseRecordModel record = new GlucoseRecordModel();
        record.setDate(Calendar.getInstance().getTime());

        try {
            double input = Double.parseDouble(glucoseField.getText().toString());
            record.setValue(input);
        } catch(NumberFormatException nfe) {
            return;
        }

        // Persist your data in a transactions
        realm.beginTransaction();
        realm.copyToRealm(record); // Persist unmanaged objects
        realm.commitTransaction();
    }

    private void gotoNextActivity() {
        final RealmResults<GlucoseRecordModel> data = realm.where(GlucoseRecordModel.class).findAll();

        // determine if first start
        boolean firstStart = (data.size() == 0) ? true : false;

        Intent i;
        if (firstStart)
            i = new Intent(RecordGlucose.this, RecordWeight.class);
        else
            i = new Intent(RecordGlucose.this, Analysis.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_glucose);
        continueButton = (Button) findViewById(R.id.btnContinue);
        glucoseField = (EditText) findViewById(R.id.glucoseValue);

        // Initialize Realm
        Realm.init(this);

        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeCurrentValue();
                gotoNextActivity();
            }
        });
    }
}
