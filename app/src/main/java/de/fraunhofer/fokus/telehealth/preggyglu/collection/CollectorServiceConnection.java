package de.fraunhofer.fokus.telehealth.preggyglu.collection;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import de.fraunhofer.fokus.telehealth.preggyglu.RecordActivity;

/**
 * Specialized service connection, which is drawn between the user interface and the service wo
 * collects data from several devices
 */
public class CollectorServiceConnection implements ServiceConnection {

    // the parental service consumer
    private RecordActivity recordActivity;
    private final String TAG = CollectorServiceConnection.class.getSimpleName();

    public CollectorServiceConnection(RecordActivity recordActivity) {
        this.recordActivity = recordActivity;
    }

    /**
     * Passes the new data collector service instance to the parent consumer.
     * @param componentName
     * @param service
     */
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service) {
        CollectorService collectorService = ((CollectorService.LocalBinder) service).getService();
        recordActivity.setCollectorService(collectorService);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        recordActivity.setCollectorService(null);
    }
}
